BIN_DIR=_output/bin
RELEASE_DIR=_output/release
REL_OSARCH_ROOT=linux/
REPO_PATH=

IMAGE_PREFIX=harbor.sigsus.cn:8443/sz_gongdianju/apulistech
IMAGE_NAME=a910-device-plugin
TAG=devel3


.EXPORT_ALL_VARIABLES:

ARMARCH=arm64
X86ARCH=amd64
MACHINE_ARCH=$(shell uname -m)

ifeq ($(MACHINE_ARCH),aarch64)
     TAG=$(TAG)-arm64
endif


all: device-plugin

init: 
	 mkdir -p ${BIN_DIR}
	 mkdir -p ${RELEASE_DIR}

image: 
	 docker build . -t $(IMAGE_PREFIX)/${IMAGE_NAME}:$(TAG) -f ./Dockerfile

pushimage: 
	 docker build . -t $(IMAGE_PREFIX)/${IMAGE_NAME}:$(TAG) -f ./Dockerfile
	 docker push $(IMAGE_PREFIX)/${IMAGE_NAME}:$(TAG) 

clean:
	 rm -rf _output/
	 rm -f *.log
